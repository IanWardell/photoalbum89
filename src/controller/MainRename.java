package controller;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

/**
 * 
 * @author Rushiraj
 * @author Ian Wardell
 * 
 * This is the contorller for the window for renaming albums
 *
 */
public class MainRename {

	@FXML
	Button submit;
	
	@FXML
	TextField newName;
	
	String user;
	String original;
	
	File oldFolder;
	File newFolder;
	
	public void rename(String user, String original) {
			this.user = user;
			this.original = original;
	}
	
	public void submit(ActionEvent event) {
		
		oldFolder = new File("Users/" + user + "/" + original);
		newFolder = new File("Users/" + user + "/" + newName.getText());
		
		if (newFolder.exists()) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText("Same album name already exists");
			alert.showAndWait();
		}
		
		if (oldFolder.renameTo(newFolder)) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Name changed");
			alert.setHeaderText("You can close the edit window now");
			alert.showAndWait();
		}

	}
	
	
}
