package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * @author Rushiraj
 * @author Ian Wardell
 * 
 * This is the controller for the album window.
 * This window appears when the user selects a album.
 * 
 * User is able to:
 * Add a photo
 * Remove a photo
 * Caption/Recaption a photo
 * Add a tag
 * Delete a tag
 * Copy a photo to another album
 * Move a photo to another album
 * 
 * The selected photo will appear in the center, if no photo is selected
 * then the first photo is automatically shown. 

 * 
 */
public class MainAlbum {
	
	@FXML
	ScrollPane root = new ScrollPane();
	
	@FXML
	TilePane tile = new TilePane();

	String user;
	String albumName;
	String path;
	ArrayList<File> photos;

	/**
	 * Get Exsisitng user albums if any
	 * Loads exsisting photos if any
	 * @param user
	 * @param albumName
	 */
	public void initialize(String user, String albumName) {
		this.user = user;
		this.albumName = albumName;

		this.path = "Users/" + user + "/" + albumName + "/";

		File album = new File(path);
		File[] files = album.listFiles();

		photos = new ArrayList<File>();
		for (int i = 0; i < files.length; i++) {
			if (getFileExtension(files[i]).equals("png") || getFileExtension(files[i]).equals("jpg")) {
				photos.add(files[i]);
			}
		}

		tile.setPadding(new Insets(15, 15, 15, 15));
		tile.setHgap(15);

		for (final File photo : photos) {
			ImageView imageView;
			imageView = createImageView(photo);
			tile.getChildren().addAll(imageView);
		}

	}

	/**
	 * Create an imageview with a picture file
	 * Double clicking a picture opens a new window that shows the picture 
	 * @param imageFile
	 * @return 
	 */
	private ImageView createImageView(File imageFile) {
		ImageView imageView = null;
		try {
			final Image image = new Image(new FileInputStream(imageFile), 150, 0, true, true);
			imageView = new ImageView(image);
			imageView.setFitWidth(150);
			imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent mouseEvent) {

					if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {

						if (mouseEvent.getClickCount() == 2) {
							try {
								BorderPane borderPane = new BorderPane();
								ImageView imageView = new ImageView();
								Image image = new Image(new FileInputStream(imageFile));
								imageView.setImage(image);
								imageView.setStyle("-fx-background-color: BLACK");
								imageView.setFitHeight(500 - 10);
								imageView.setPreserveRatio(true);
								imageView.setSmooth(true);
								imageView.setCache(true);
								borderPane.setCenter(imageView);
								borderPane.setStyle("-fx-background-color: BLACK");
								Stage newStage = new Stage();
								newStage.setTitle(imageFile.getName());
								Scene scene = new Scene(borderPane, Color.BLACK);
								newStage.setScene(scene);
								newStage.show();
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}

						}
					}
				}
			});
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		return imageView;
	}

	/**
	 * Gets the file extension 
	 * @param file
	 * @return the file extension
	 */
	private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }

}
