package controller;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import object.User;

/**
 * @author Rushiraj
 * @author IanWardell
 * 
 * This is the main window (subsystem) for the admin.
 * 
 * Admin can do any of the following: 
 * List users
 * Create a new user
 * Delete an existing user
 *
 */
public class MainAdmin {
	
	@FXML
	Button buttonAddUser;
	
	@FXML
	Button buttonDeleteUser;
	
	@FXML
	Button adminLogout;
	
	@FXML
	Button adminQuit;
	
	@FXML
	ListView<User> userList = new ListView<User>();
	
	@FXML
	Label lblAdminStatus;
	
	@FXML
	TextField manageUser;
	
	ObservableList<User> users = FXCollections.observableArrayList();
	
	@FXML
	public void initialize() {
		File usersFolder = new File("Users");
		String[] folders = usersFolder.list(new FilenameFilter() {
			  @Override
			  public boolean accept(File current, String name) {
			    return new File(current, name).isDirectory();
			  }
		});
		
		for(int i = 0; i< folders.length; i++) {
			User user = new User(folders[i].trim());
			users.add(user);
			userList.setItems(users);
		}

	}
	/**
	 * Create a directory for a new user
	 * 
	 * @param event
	 */
	public void addUser(ActionEvent event) {
		User newUser = new User(manageUser.getText().trim());
		users.add(newUser);
		userList.setItems(users);
		manageUser.clear();
		lblAdminStatus.setText("User " + newUser.getUsername() + " added");
	
		createFolder(newUser.getUsername());

	}
	
	/**
	 * Delete the selected users directory, this also deletes all of the users photos
	 * @param event
	 */
	public void deleteUser(ActionEvent event) {
		User userToDelete = userList.getSelectionModel().getSelectedItem();
		users.remove(userToDelete);
		userList.setItems(users);
		lblAdminStatus.setText("User " + userToDelete.getUsername() + " removed");
		
		File file = new File("Users/" + userToDelete.getUsername());
		deleteFolder(file);
		
	}
	
	/**
	 * Gracefully logout
	 * @param event
	 * @throws IOException
	 */
	public void logout(ActionEvent event) throws IOException {
		((Node) event.getSource()).getScene().getWindow().hide();
		Stage primaryStage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
		Scene scene = new Scene(root,500,500);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	/**
	 * Gracefully quit app
	 * @param event
	 */
	public void quit(ActionEvent event) {
		Platform.exit();
		System.exit(0);
	}

	/**
	 * Create a directory for the user
	 * 
	 * @param username
	 */
	public void createFolder(String username) {
		File file = new File("Users/" + username);
		if (!file.exists()) {
			file.mkdir();
		}

	}

	/**
	 * Delete a user directory
	 * 
	 * @param username
	 */
	public void deleteFolder(File dir) {
	    if (dir.isDirectory()) {
	        File[] files = dir.listFiles();
	        if (files != null && files.length > 0) {
	            for (File aFile : files) {
	            	deleteFolder(aFile);
	            }
	        }
	        dir.delete();
	    } else {
	        dir.delete();
	    }
	}

}
