package controller;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import object.Album;
import object.User;

/**
 * @author Rushiraj
 * @author Ian Wardell
 *
 * This is the controller for the main user window.
 * This window will display user albums.
 * User is able to: Create an album Delete an album Rename an album Open
 * an album Search for a photo
 *
 */
public class MainUser {

	@FXML
	Button buttonOpenAlbum;

	@FXML
	Button buttonRenameAlbum;

	@FXML
	Button buttonDeleteAlbum;

	@FXML
	Button buttonCreateAlbum;

	@FXML
	Button buttonSearch;

	@FXML
	Button buttonUserLogout;

	@FXML
	Button buttonUserQuit;

	@FXML
	ListView<Album> albumList = new ListView<Album>();

	@FXML
	TextField txtSearch;

	@FXML
	TextField txtCreate;

	@FXML
	Label usernameWelcome;

	ObservableList<Album> albums = FXCollections.observableArrayList();

	User currentUser;

	/**
	 * Iniatlaize the inital list of users if any
	 * @param user
	 */
	public void initialize(String user) {

		currentUser = new User(user);
		usernameWelcome.setText("Welcome " + currentUser.getUsername());

		File albumFolders = new File("Users/" + currentUser.getUsername());
		String[] folders = albumFolders.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});

		for (int i = 0; i < folders.length; i++) {
			Album album = new Album(folders[i].trim());
			albums.add(album);
			albumList.setItems(albums);
		}

	}
	
	/**
	 * Opens a new window with the photos 
	 * @param event
	 * @throws IOException
	 */
	public void openAlbum(ActionEvent event) throws IOException {
		Album albumToOpen = albumList.getSelectionModel().getSelectedItem();

		Stage primaryStage = new Stage();
		FXMLLoader loader = new FXMLLoader();
		Pane root = loader.load(getClass().getResource("/fxml/Album.fxml").openStream());

		MainAlbum mainAlbum = (MainAlbum) loader.getController();

		mainAlbum.initialize(currentUser.getUsername(), albumToOpen.AlbumName);

		Scene scene = new Scene(root, 500, 500);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	/**
	 * Deletes the foder containg the photos from directory 
	 * @param event
	 */
	public void deleteAlbum(ActionEvent event) {

		Album albumToDelete = albumList.getSelectionModel().getSelectedItem();
		albums.remove(albumToDelete);
		albumList.setItems(albums);

		File file = new File("Users/" + currentUser.getUsername() + "/" + albumToDelete.getAlbumName());
		deleteFolder(file);

	}

	/**
	 * Actual function that does the deletion
	 * @param dir
	 */
	public void deleteFolder(File dir) {
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			if (files != null && files.length > 0) {
				for (File aFile : files) {
					deleteFolder(aFile);
				}
			}
			dir.delete();
		} else {
			dir.delete();
		}
	}

	/**
	 * Creates a directory under the user for its new album
	 * @param event
	 */
	public void createAlbum(ActionEvent event) {

		File file = new File("Users/" + currentUser.getUsername() + "/" + txtCreate.getText());

		if (!file.exists()) {
			if (file.mkdir()) {

				Album newAlbum = new Album(txtCreate.getText());
				albums.add(newAlbum);
				albumList.setItems(albums);
				txtCreate.clear();

				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Album created");
				alert.showAndWait();

			} else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Couldn't create album");
				alert.showAndWait();
			}

		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Album already exists");
			alert.setHeaderText("Choose a different name");
			alert.showAndWait();
		}

	}

	/**
	 * Opens a new window that allows the user to rename a album.
	 * Album list is then refreashed
	 * @param event
	 * @throws IOException
	 */
	public void renameAlbum(ActionEvent event) throws IOException {
		Stage primaryStage = new Stage();
		FXMLLoader loader = new FXMLLoader();
		Pane root = loader.load(getClass().getResource("/fxml/Rename.fxml").openStream());

		MainRename mainRename = (MainRename) loader.getController();
		Album albumToRename = albumList.getSelectionModel().getSelectedItem();
		mainRename.rename(currentUser.getUsername(), albumToRename.getAlbumName());

		Scene scene = new Scene(root, 393, 240);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				albums.clear();
				File albumFolders = new File("Users/" + currentUser.getUsername());
				String[] folders = albumFolders.list(new FilenameFilter() {
					@Override
					public boolean accept(File current, String name) {
						return new File(current, name).isDirectory();
					}
				});

				for (int i = 0; i < folders.length; i++) {
					Album album = new Album(folders[i].trim());
					albums.add(album);
					albumList.setItems(albums);
				}
			}
		});
	}

	
	/**
	 * Gracefully logout of app
	 * @param event
	 */
	public void logout(ActionEvent event) throws IOException {
		((Node) event.getSource()).getScene().getWindow().hide();
		Stage primaryStage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
		Scene scene = new Scene(root, 500, 500);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Gracefully quit app
	 * @param event
	 */
	public void quit(ActionEvent event) {
		Platform.exit();
		System.exit(0);
	}

}
