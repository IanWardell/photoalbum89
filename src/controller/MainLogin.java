package controller;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author Rushiraj
 * @author Ian Wardell
 * 
 * This is the window for the login system.
 *
 */
public class MainLogin {

		@FXML
		private Label lblStatus;

		@FXML
		private TextField txtUsername;

		/**
		 * User can either be admin or user
		 * Inital users are Rushi and Ian
		 * @param event
		 * @throws IOException
		 */
		public void Login(ActionEvent event) throws IOException {

			if (txtUsername.getText().equals("admin")) {

				((Node) event.getSource()).getScene().getWindow().hide();
				Stage primaryStage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/fxml/Admin.fxml"));
				Scene scene = new Scene(root,500,500);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();

			} else {

			File file = new File("Users/" + txtUsername.getText());

			if (file.exists()) {

				((Node) event.getSource()).getScene().getWindow().hide();
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/fxml/User.fxml").openStream());

				MainUser mainUser = (MainUser) loader.getController();
				mainUser.initialize(txtUsername.getText());

				Scene scene = new Scene(root, 500, 500);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();

			} else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("User not found");
				alert.setHeaderText("Go to admin window to add user");
				alert.showAndWait();
			}

		}
	}
}