package object;

/**
 * @author Rushiraj
 * @author IanWardell
 * 
 * Album object
 *
 */
public class Album {

	//Insert code here

	public String AlbumName;

	public Album(String AlbumName) {
		this.AlbumName = AlbumName;
	}

	public String getAlbumName() {
		return this.AlbumName;
	}

	@Override
	public String toString() {
		return getAlbumName();
	}


}