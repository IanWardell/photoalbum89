package object;
import java.io.Serializable;
/*
 * @author Rushiraj
 * @author IanWardell
 */
import java.util.ArrayList;

/**
 * 
 * @author Rushiraj
 * @author Ian Wardell
 * 
 * Photo object that needs to be serialiable
 *
 */
public class Photo implements Serializable {

	String path;
	String caption;
	String date; 
	ArrayList<String> tags; 
	

	public Photo(String path) {
		this.path = path;
		tags = new ArrayList<String>();
	}


}
